# README #

CheckSign win utility for WDDiscovery project

### Usage
You can launch CheckSign from connad line as following:
CheckSign path_to_file_to_check_code_signing

As a result CheckSign utility returns one of the followign codes:
-1 - path parameter missed
 0 - file is signed and the signature was verified
 1 - file is not signed
 2 - unknown error occurred trying to verify the signature
 3 - signature is present, but specifically disallowed
 4 - signature is present, but not trusted
 5 - no signature, publisher or timestamp errors
 6 - other errors
